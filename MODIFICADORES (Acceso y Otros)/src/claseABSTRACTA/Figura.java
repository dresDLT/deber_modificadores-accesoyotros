package claseABSTRACTA;

public abstract class Figura {
	
	protected String nombreFigura;
	protected static int numeroDeFigurasCreadas;
	
	public Figura(String nombreFigura) {
		this.nombreFigura=nombreFigura;
		++numeroDeFigurasCreadas;
	}

	public abstract double calcularArea();
	
}
