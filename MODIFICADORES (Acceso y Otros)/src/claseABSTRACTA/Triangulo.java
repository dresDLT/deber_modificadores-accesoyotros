package claseABSTRACTA;

public class Triangulo extends Figura {

	protected double base, altura;
	
	public Triangulo(double base, double altura) {
		super("TRIANGULO");
		this.base=base;
		this.altura=altura;
	}

	@Override
	public double calcularArea() {
		return (this.base*this.altura/2);
	}
	
}
