package claseABSTRACTA;

public class Main {

	public static void main(String[] args) {
		
		Figura [] misFiguras = new Figura [3];
		
		misFiguras [0]= new Cuadrado(5); 		//lado
		misFiguras [1]= new Circulo(5);	 		//Radio
		misFiguras [2]= new Triangulo(5, 5);	//Base, Altura
		
		System.out.println("misFiguras [0]= new Cuadrado(5);"+" "+"		//lado \n"
				+ "misFiguras [1]= new Circulo(5);"+" "+"		//Radio \n"
				+ "misFiguras [2]= new Triangulo(5, 5);"+" "+"		//Base, Altura");
		
		
		System.out.println("\nSE HAN CREADO: "+Figura.numeroDeFigurasCreadas+" FIGURAS\n");
		
		for (Figura figura : misFiguras) {
			System.out.println("FIGURA: "+figura.nombreFigura+" "
					+"	AREA: "+figura.calcularArea());
		}
		


	}

}
