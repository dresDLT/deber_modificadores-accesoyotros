package claseABSTRACTA;

public final class Circulo extends Figura{
	
	protected double radio;

	public Circulo(double radio) {
		super("CIRCULO");
		this.radio=radio;
	}

	@Override
	public double calcularArea() {
		return (Math.PI*this.radio*this.radio);
	}
	
	

}
