package claseABSTRACTA;

public final class Cuadrado extends Figura{
	
	protected double lado;

	public Cuadrado(double lado) {
		super("CUADRADO");
		this.lado=lado;
	}

	@Override
	public double calcularArea() {
		return Math.pow(this.lado, 2);
	}

}
