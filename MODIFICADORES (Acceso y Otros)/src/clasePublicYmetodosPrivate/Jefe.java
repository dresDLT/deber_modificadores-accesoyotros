package clasePublicYmetodosPrivate;

public class Jefe extends Empleado{

	private String strSeccionEncargada;
	
	public Jefe(String strNombre, int intEdad, double dblSueldo, String strSeccionEncargada) {
		super(strNombre, intEdad, dblSueldo);
		// TODO Auto-generated constructor stub
		this.strSeccionEncargada=strSeccionEncargada;
	}
	
	public double calcularPago(double dblCantidadVendida){
		return (getDblSueldo() + dblCantidadVendida * 0.1);
		
	}
	
}
