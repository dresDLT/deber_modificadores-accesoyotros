package clasePublicYmetodosPrivate;

public class Empleado {
	private String strNombre;
	private int intEdad;
	private double dblSueldo;
	public static int numeroDeEmpleados;
		
	public Empleado(String strNombre, int intEdad, double dblSueldo) {
		// TODO Auto-generated constructor stub
		this.strNombre = strNombre;
		this.intEdad = intEdad;
		this.dblSueldo = dblSueldo;
		
		++numeroDeEmpleados;
	}
	
	//Getter y Setter strNombre
	public String getStrNombre() {
		return strNombre;
	}
	
	public void setStrNombre(String strNombre) {
		this.strNombre = strNombre;
	}
	
	//Getter y Setter intEdad
	public int getIntEdad() {
		return intEdad;
	}
	
	public void setIntEdad(int intEdad) {
		this.intEdad = intEdad;
	}
	
	//Getter y Setter dblSueldo
	public double getDblSueldo() {
		return dblSueldo;
	}
	
	public void setDblSueldo(double dblSueldo) {
		this.dblSueldo = dblSueldo;
	}
	
	//calcular pago
	public double calcularPago(double dblCantidadVendida){
		return (dblSueldo + dblCantidadVendida*0.05);
		
	}
	
	//metodo subir sueldo
	public void subirSueldo(double dblPorcentaje){
		this.dblSueldo += (this.dblSueldo * (dblPorcentaje/100));
	}
	
}
