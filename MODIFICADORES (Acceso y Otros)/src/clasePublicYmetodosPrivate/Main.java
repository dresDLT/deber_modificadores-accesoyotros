package clasePublicYmetodosPrivate;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Empleado[] misEmpleados = new Empleado [10];
		misEmpleados[0] = new Empleado("Andr�s", 25, 375.7);
		misEmpleados[1] = new Empleado("Rosario", 26, 380.0);
		misEmpleados[2] = new Jefe("Mar�a", 43, 450.5, "Seccion Deportiva");
		misEmpleados[3] = new Jefe("Jos�", 40, 460.5, "Seccion Contable");
		misEmpleados[4] = new Empleado("Rosa", 26, 380.0);
		
		double dblCantidadVendida = 200.0;
		
		System.out.println("PAGOS -->\n");
		
		for (int i=0; i<Empleado.numeroDeEmpleados; i++) {
			System.out.println(misEmpleados[i].getStrNombre()+"	"+"  Sueldo: "+misEmpleados[i].getDblSueldo()+"   Cantidad Vendida: "+dblCantidadVendida+"    ---> PAGO: "+misEmpleados[i].calcularPago(dblCantidadVendida));
		}
		
		System.out.println("\nSUBIR SUELDO A JEFE --> Mar�a UN PORCENTAJE DE 50%\n");
		misEmpleados[2].subirSueldo(50);
		
		System.out.println("PAGOS -->\n");
		
		for (int i=0; i<Empleado.numeroDeEmpleados; i++) {
			System.out.println(misEmpleados[i].getStrNombre()+"	"+"  Sueldo: "+misEmpleados[i].getDblSueldo()+"   Cantidad Vendida: "+dblCantidadVendida+"    ---> PAGO: "+misEmpleados[i].calcularPago(dblCantidadVendida));
		}
		
		
		
		
		
		
	}

}
